package org.eyo.bookmanager.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserCommentsResponseDTO {

    private Long bookId;
    private Long id;
    private String content;
    private Float punctuation;
}
