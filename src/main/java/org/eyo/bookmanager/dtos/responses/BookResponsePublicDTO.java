package org.eyo.bookmanager.dtos.responses;

import lombok.Getter;
import lombok.Setter;
import org.eyo.bookmanager.models.Book;

@Getter
@Setter
public class BookResponsePublicDTO {

    public BookResponsePublicDTO(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
    }

    public BookResponsePublicDTO(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    private Long id;
    private String title;
}
