package org.eyo.bookmanager.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CommentRequestDto {

    private String nick;
    private String content;
    private Float punctuation;

}
