package org.eyo.bookmanager.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.eyo.bookmanager.models.Book;

@Getter
@AllArgsConstructor
public class BookRequestDTO {

    public BookRequestDTO(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.review = book.getReview();
        this.author = book.getAuthor();
        this.editorial = book.getEditorial();
        this.yearPublication = book.getYearPublication();
    }

    private Long id;
    private String title;
    private String review;
    private String author;
    private String editorial;
    private Long yearPublication;

}
