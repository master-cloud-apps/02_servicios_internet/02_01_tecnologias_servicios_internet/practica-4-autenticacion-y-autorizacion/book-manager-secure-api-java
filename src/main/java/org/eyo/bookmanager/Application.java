package org.eyo.bookmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
        "org.eyo.bookmanager.controllers", "org.eyo.bookmanager.services",
        "org.eyo.bookmanager.repositories", "org.eyo.bookmanager.repositories.db",
        "org.eyo.bookmanager.security"
})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
