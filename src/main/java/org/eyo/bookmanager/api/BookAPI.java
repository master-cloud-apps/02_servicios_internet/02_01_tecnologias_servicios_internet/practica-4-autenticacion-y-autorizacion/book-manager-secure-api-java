package org.eyo.bookmanager.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.eyo.bookmanager.dtos.requests.BookRequestDTO;
import org.eyo.bookmanager.dtos.responses.BookResponseDTO;
import org.eyo.bookmanager.dtos.responses.BookResponsePublicDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;
import java.util.Collection;

public interface BookAPI {

    @Operation(summary = "Create a book")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Book created",
                    headers = @Header(name = "Location", description = "Location of the book", schema =
                    @Schema(implementation = String.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            )
    })
    ResponseEntity<Void> createBook(@RequestBody BookRequestDTO newBook);

    @Operation(summary = "Get all the books")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Returns all the books",
                    content = @Content(mediaType = "application/json", array = @ArraySchema(schema =
                    @Schema(implementation = BookResponsePublicDTO.class)))
            )
    })
    ResponseEntity<Collection<BookResponsePublicDTO>> getBooks(Principal principal);

    @Operation(summary = "Delete a book by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Book deleted"
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    ResponseEntity<Void> deleteBookById(@PathVariable Long bookId);

    @Operation(summary = "Get a book by id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Book selected",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = BookResponseDTO.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Book not found",
                    content = @Content
            )
    })
    ResponseEntity<BookResponseDTO> getBookById(@PathVariable Long bookId);
}
