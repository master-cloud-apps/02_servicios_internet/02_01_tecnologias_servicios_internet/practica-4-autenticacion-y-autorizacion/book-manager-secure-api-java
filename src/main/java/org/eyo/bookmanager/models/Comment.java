package org.eyo.bookmanager.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String content;
    private String name;
    private Float punctuation;

    @ManyToOne
    private User user;

    @ManyToOne
    private Book book;

}
