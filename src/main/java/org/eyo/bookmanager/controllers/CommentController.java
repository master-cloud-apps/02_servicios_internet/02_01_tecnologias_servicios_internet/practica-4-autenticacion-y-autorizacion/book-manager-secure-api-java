package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.api.CommentAPI;
import org.eyo.bookmanager.dtos.requests.CommentRequestDto;
import org.eyo.bookmanager.dtos.responses.CommentResponseDTO;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.models.User;
import org.eyo.bookmanager.services.BookService;
import org.eyo.bookmanager.services.CommentService;
import org.eyo.bookmanager.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/books/{bookId}/comments")
public class CommentController implements CommentAPI {

    private CommentService commentService;
    private BookService bookService;
    private UserService userService;

    public CommentController(CommentService commentService, BookService bookService, UserService userService) {
        this.commentService = commentService;
        this.bookService = bookService;
        this.userService = userService;
    }

    @PostMapping("")
    public ResponseEntity<Void> createComment(@PathVariable Long bookId,
                                              @RequestBody CommentRequestDto commentRequest, Principal principal) {
        if (this.userService.getUserByNick(principal.getName()) == null || this.bookService.findById(bookId) == null) {
            return ResponseEntity.notFound().build();
        }
        Comment comment = new Comment();
        comment.setContent(commentRequest.getContent());
        comment.setPunctuation(commentRequest.getPunctuation());
        User user = this.userService.getUserByNick(principal.getName());
        comment.setUser(user);
        comment.setBook(this.bookService.findById(bookId));
        this.commentService.save(bookId, comment);

        URI location =
                fromCurrentRequest().path("/{id}").buildAndExpand(comment.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{commentId}")
    public ResponseEntity<CommentResponseDTO> getCommentById(@PathVariable Long bookId, @PathVariable Long commentId) {
        if (!existBookAndComment(bookId, commentId)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(new CommentResponseDTO(this.commentService.finById(commentId)));
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> deleteCommentOverBook(@PathVariable Long bookId, @PathVariable Long commentId) {
        if (!existBookAndComment(bookId, commentId)) {
            return ResponseEntity.notFound().build();
        }
        this.commentService.deleteComment(bookId, commentId);
        return ResponseEntity.noContent().build();
    }

    private boolean existBookAndComment(Long bookId, Long commentId) {
        return this.commentService.finById(commentId) != null && this.bookService.findById(bookId) != null;
    }
}
