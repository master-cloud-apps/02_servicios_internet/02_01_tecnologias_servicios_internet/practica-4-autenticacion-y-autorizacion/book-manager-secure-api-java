package org.eyo.bookmanager.repositories.db;

import org.eyo.bookmanager.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentJpaRepository extends JpaRepository<Comment, Long> {
}
