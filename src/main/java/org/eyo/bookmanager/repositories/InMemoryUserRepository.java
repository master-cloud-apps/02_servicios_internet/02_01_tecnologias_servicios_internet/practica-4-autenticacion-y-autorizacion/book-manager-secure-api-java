package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.User;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Profile("test")
@Repository
public class InMemoryUserRepository implements UserRepository {
    private AtomicLong nextId = new AtomicLong(1);
    private ConcurrentMap<Long, User> users = new ConcurrentHashMap<>();

    @Override
    public void save(User user) {
        if (user.getId() == null || user.getId() == 0) {
            user.setId(nextId.getAndIncrement());
        }
        this.users.put(user.getId(), user);
    }

    @Override
    public Collection<User> getAll() {
        return this.users.values();
    }

    @Override
    public User findById(Long userId) {
        return this.users.get(userId);
    }

    @Override
    public void deleteById(Long userId) {
        this.users.remove(userId);
    }

    @Override
    public User getUserByNick(String nick) {
        return this.users.values().stream()
                .filter(user -> user.getNick().equals(nick))
                .findAny()
                .orElse(null);
    }

    @Override
    public User getUserByEmail(String email) {
        return this.users.values().stream()
                .filter(user -> user.getEmail().equals(email))
                .findAny()
                .orElse(null);
    }

}
