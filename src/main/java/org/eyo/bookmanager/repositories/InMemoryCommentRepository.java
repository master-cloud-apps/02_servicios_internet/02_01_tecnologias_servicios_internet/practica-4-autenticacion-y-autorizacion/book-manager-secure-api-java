package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Comment;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Profile("test")
@Repository
public class InMemoryCommentRepository implements CommentRepository{

    private AtomicLong nextId = new AtomicLong(1);
    private ConcurrentMap<Long, Comment> comments = new ConcurrentHashMap<>();

    @Override
    public Collection<Comment> findAll(Long bookId) {
        return this.comments.values();
    }

    @Override
    public void save(Long bookId, Comment comment) {
        if (comment.getId() == null || comment.getId() == 0) {
            comment.setId(nextId.getAndIncrement());
        }
        this.comments.put(comment.getId(), comment);
    }

    @Override
    public void deleteById(Long commentId) {
        this.comments.remove(commentId);
    }

    @Override
    public Comment finById(Long commentId) {
        return this.comments.get(commentId);
    }
}
