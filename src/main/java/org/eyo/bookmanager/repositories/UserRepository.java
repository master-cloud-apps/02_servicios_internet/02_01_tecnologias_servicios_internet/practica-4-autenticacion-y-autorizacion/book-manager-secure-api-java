package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.User;

import javax.swing.*;
import java.util.Collection;

public interface UserRepository {
    void save(User user);

    Collection<User> getAll();

    User findById(Long userId);

    void deleteById(Long userId);

    User getUserByNick(String nick);

    User getUserByEmail(String email);
}
