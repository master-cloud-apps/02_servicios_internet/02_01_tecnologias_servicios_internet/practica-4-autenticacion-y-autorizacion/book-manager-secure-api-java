package org.eyo.bookmanager.services;

import org.eyo.bookmanager.models.User;

import java.util.Collection;

public interface UserService {

    void save(User user);

    Collection<User> getAll();

    User findById(Long userId);

    void deleteById(Long userId);

    User getUserByNick(String nick);
}
