package org.eyo.bookmanager.controllers;


import org.eyo.bookmanager.dtos.requests.BookRequestDTO;
import org.eyo.bookmanager.dtos.responses.BookResponseDTO;
import org.eyo.bookmanager.models.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;


class BookControllerDBTest extends ControllerTest {


    @Test
    void create_book_should_return_location() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        this.deleteBooks(Arrays.asList(getIdFromLocation(bookResponse.getHeaders().get("location").get(0))));

        assertEquals(HttpStatus.CREATED, bookResponse.getStatusCode());
        assertNotNull(bookResponse.getHeaders().get("location"));
        assertTrue(bookResponse.getHeaders().get("location").get(0).contains("http://localhost/"));
    }

    @Test
    void when_call_all_books_should_return_list_of_books() {
        ResponseEntity bookListResponse = this.bookController.getBooks(null);

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
    }

    @Test
    void given_2_books_created_when_call_all_books_should_return_list_of_books_len_2() {
        ResponseEntity bookResponse1 = this.bookController.createBook(createTestBook(new Book("Test title")));
        ResponseEntity bookResponse2 = this.bookController.createBook(createTestBook(new Book("Test title 2")));

        ResponseEntity bookListResponse = this.bookController.getBooks(null);

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
        assertEquals(2, ((Collection) bookListResponse.getBody()).size());

        this.deleteBooks(Arrays.asList(
                getIdFromLocation(bookResponse1.getHeaders().get("location").get(0)),
                getIdFromLocation(bookResponse2.getHeaders().get("location").get(0))
        ));
    }

    @Test
    void given_1_book_created_when_call_all_books_should_return_book_only_id_and_title() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));

        ResponseEntity bookListResponse = this.bookController.getBooks(null);
        this.deleteBooks(Arrays.asList(getIdFromLocation(bookResponse.getHeaders().get("location").get(0))));

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
    }

    @Test
    void given_1_book_created_when_call_book_by_id_should_return_the_comments() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromResponse(bookResponse);

        ResponseEntity<BookResponseDTO> bookResponseId = this.bookController.getBookById(bookId);
        this.deleteBooks(Arrays.asList(bookId));

        assertEquals("Test title", bookResponseId.getBody().getTitle());
        assertTrue(bookResponseId.getBody().getId() >= 1L);
        assertEquals(0, bookResponseId.getBody().getComments().size());
    }

    @Test
    void given_0_books_created_when_call_book_by_id_should_return_not_found() {
        ResponseEntity<BookResponseDTO> bookResponseId = this.bookController.getBookById(-1L);

        assertEquals(HttpStatus.NOT_FOUND, bookResponseId.getStatusCode());
    }
}
