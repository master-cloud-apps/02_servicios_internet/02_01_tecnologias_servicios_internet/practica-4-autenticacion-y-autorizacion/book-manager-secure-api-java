package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.PrincipalTestDTO;
import org.eyo.bookmanager.dtos.requests.CommentRequestDto;
import org.eyo.bookmanager.dtos.responses.CommentResponseDTO;
import org.eyo.bookmanager.models.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CommentControllerTest extends ControllerTest {

    @Test
    void should_return_location_when_create_comment_over_book() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createUser("email_1", "nick_1"));
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromLocation(bookResponse.getHeaders().get("location").get(0));

        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto("nick_1",
                "", 0F), new PrincipalTestDTO("nick_1"));
        this.deleteBooks(Arrays.asList(bookId));
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(HttpStatus.CREATED, commentResponse.getStatusCode());
        assertNotNull(commentResponse.getHeaders().get("location"));
    }

    @Test
    void when_create_comment_over_non_existing_book_should_return_not_found() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());

        ResponseEntity commentResponse = this.commentController.createComment(1000L, new CommentRequestDto(TEST_NICK,
                "", 0F), this.principal);
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(HttpStatus.NOT_FOUND, commentResponse.getStatusCode());
    }

    @Test
    void given_created_comment_when_get_comment_by_id_should_return_comment() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        ResponseEntity commentResponse = this.commentController.createComment(getIdFromResponse(bookResponse),
                new CommentRequestDto(TEST_NICK, "test_comment", 0F), this.principal);

        ResponseEntity<CommentResponseDTO> commentGetByIdResponse = this.commentController.getCommentById(getIdFromResponse(bookResponse), getIdFromResponse(commentResponse));
        this.deleteBooks(Arrays.asList(getIdFromResponse(bookResponse)));
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(HttpStatus.OK, commentGetByIdResponse.getStatusCode());
        assertEquals("test_comment", commentGetByIdResponse.getBody().getContent());
    }

    @Test
    void should_return_no_content_when_delete_comment_created() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createUser("email", "nick"));
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto("nick",
                "", 0F), new PrincipalTestDTO("nick"));
        Long commentId = getIdFromResponse(commentResponse);

        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(bookId, commentId);

        this.deleteBooks(Arrays.asList(bookId));
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(HttpStatus.NO_CONTENT, commentDeletedResponse.getStatusCode());
    }

    @Test
    void should_return_not_found_when_delete_comment_not_exist() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromResponse(bookResponse);

        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(bookId, -1L);

        this.deleteBooks(Arrays.asList(bookId));

        assertEquals(HttpStatus.NOT_FOUND, commentDeletedResponse.getStatusCode());
    }

    @Test
    void should_return_not_found_when_delete_comment_not_exist_and_book() {
        ResponseEntity commentDeletedResponse = this.commentController.deleteCommentOverBook(-1L, -1L);

        assertEquals(HttpStatus.NOT_FOUND, commentDeletedResponse.getStatusCode());
    }

    @Test
    void when_create_comment_over_not_existing_user_should_return_not_found() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title 2")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity createCommentResponse = this.commentController.createComment(bookId, new CommentRequestDto(
                "not_found_nick", "", 4.5F), this.principal);

        assertEquals(HttpStatus.NOT_FOUND, createCommentResponse.getStatusCode());

        this.bookController.deleteBookById(bookId);
    }


}
