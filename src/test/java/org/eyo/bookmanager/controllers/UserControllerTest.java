package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.dtos.requests.CommentRequestDto;
import org.eyo.bookmanager.dtos.requests.UserRequestDTO;
import org.eyo.bookmanager.dtos.responses.UserCommentsResponseDTO;
import org.eyo.bookmanager.dtos.responses.UserResponseDTO;
import org.eyo.bookmanager.models.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class UserControllerTest extends ControllerTest {


    @Test
    void create_user_should_return_ok_with_all_fields() {
        ResponseEntity<Void> response = this.userController.createUSer(createTestUser());

        assertNotNull(getIdFromResponse(response));
        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        this.userController.deleteUserById(getIdFromResponse(response));
    }

    @Test
    void given_user_created_when_call_all_users_should_return_one_user() {
        ResponseEntity<Void> response = this.userController.createUSer(createTestUser());

        ResponseEntity<Collection<UserResponseDTO>> users = this.userController.getUsers();

        assertEquals(1, users.getBody().size());

        this.userController.deleteUserById(getIdFromResponse(response));
    }

    @Test
    void given_create_user_when_get_user_should_return_ok_with_all_fields() {
        ResponseEntity<Void> response = this.userController.createUSer(createTestUser());

        ResponseEntity<UserResponseDTO> userCreatedResponse = this.userController.getUserById(getIdFromResponse(response));

        assertNotNull(getIdFromResponse(response));
        assertEquals("test_nick", userCreatedResponse.getBody().getNick());

        this.userController.deleteUserById(getIdFromResponse(response));
    }

    @Test
    void create_two_users_same_nick_should_return_bad_response() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity<Void> userDuplicatedResponse = this.userController.createUSer(createTestUser());

        assertEquals(HttpStatus.CREATED, userCreated.getStatusCode());
        assertEquals(HttpStatus.BAD_REQUEST, userDuplicatedResponse.getStatusCode());

        this.userController.deleteUserById(getIdFromResponse(userCreated));
    }

    @Test
    void given_user_created_with_comments_when_delete_user_should_return_error() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title 2")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto(
                "test_nick", "", 4.5F), this.principal);

        ResponseEntity<Void> userDeleted = this.userController.deleteUserById(getIdFromResponse(userCreated));
        assertEquals(HttpStatus.BAD_REQUEST, userDeleted.getStatusCode());

        this.commentController.deleteCommentOverBook(bookId, getIdFromResponse(commentResponse));
        this.bookController.deleteBookById(bookId);
        userDeleted = this.userController.deleteUserById(getIdFromResponse(userCreated));
        assertEquals(HttpStatus.NO_CONTENT, userDeleted.getStatusCode());
    }

    @Test
    void given_user_created_with_comments_when_get_user_comments_should_return_one_comment() {
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title 2")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto(
                "test_nick", "", 4.5F), this.principal);

        ResponseEntity<Collection<UserCommentsResponseDTO>> userCreatedResponse = this.userController.getUserComments(getIdFromResponse(userCreated));
        assertEquals(HttpStatus.OK, userCreatedResponse.getStatusCode());
        assertEquals(1, userCreatedResponse.getBody().size());

        this.commentController.deleteCommentOverBook(bookId, getIdFromResponse(commentResponse));
        this.bookController.deleteBookById(bookId);
        this.userController.deleteUserById(getIdFromResponse(userCreated));
    }

    @Test
    void given_not_existing_user_when_get_user_comments_should_return_not_found(){
        ResponseEntity<Collection<UserCommentsResponseDTO>> userCommentsResponse = this.userController.getUserComments(-1L);

        assertEquals(HttpStatus.NOT_FOUND, userCommentsResponse.getStatusCode());

    }

    @Test
    void given_user_created_when_update_email_should_return_new_email(){
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        UserRequestDTO updatedEmailUser = new UserRequestDTO("test_nick_updated", "test_email_updated", "test_pwd");

        ResponseEntity<UserResponseDTO> userUpdatedResponse = this.userController.updateUser(getIdFromResponse(userCreated), updatedEmailUser);

        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(HttpStatus.OK, userUpdatedResponse.getStatusCode());
        assertEquals("test_nick_updated", userUpdatedResponse.getBody().getNick());
        assertEquals("test_email_updated", userUpdatedResponse.getBody().getEmail());
    }
}
