package org.eyo.bookmanager.controllers;


import org.eyo.bookmanager.dtos.requests.CommentRequestDto;
import org.eyo.bookmanager.dtos.responses.BookResponseDTO;
import org.eyo.bookmanager.dtos.responses.BookResponsePublicDTO;
import org.eyo.bookmanager.models.Book;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookControllerTest extends ControllerTest {

    @Test
    void create_book_should_return_location() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        this.deleteBooks(Arrays.asList(getIdFromLocation(bookResponse.getHeaders().get("location").get(0))));

        assertEquals(HttpStatus.CREATED, bookResponse.getStatusCode());
        assertNotNull(bookResponse.getHeaders().get("location"));
        assertTrue(bookResponse.getHeaders().get("location").get(0).contains("http://localhost/"));
    }

    @Test
    void when_call_all_books_should_return_list_of_books() {
        ResponseEntity bookListResponse = this.bookController.getBooks(null);

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
    }

    @Test
    void given_2_books_created_when_call_all_books_should_return_list_of_books_len_2() {
        ResponseEntity bookResponse1 = this.bookController.createBook(createTestBook(new Book("Test title")));
        ResponseEntity bookResponse2 = this.bookController.createBook(createTestBook(new Book("Test title 2")));

        ResponseEntity bookListResponse = this.bookController.getBooks(null);

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
        assertEquals(2, ((Collection) bookListResponse.getBody()).size());

        this.deleteBooks(Arrays.asList(
                getIdFromLocation(bookResponse1.getHeaders().get("location").get(0)),
                getIdFromLocation(bookResponse2.getHeaders().get("location").get(0))
        ));
    }

    @Test
    void given_1_book_created_when_call_all_books_should_return_book_only_id_and_title() {
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));

        ResponseEntity bookListResponse = this.bookController.getBooks(null);
        this.deleteBooks(Arrays.asList(getIdFromLocation(bookResponse.getHeaders().get("location").get(0))));

        assertEquals(HttpStatus.OK, bookListResponse.getStatusCode());
    }

    @Test
    void given_1_book_created_when_call_book_by_id_should_return_the_comments(){
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto(
                "test_nick", "", 0F), this.principal);

        ResponseEntity<BookResponseDTO> bookResponseId = this.bookController.getBookById(bookId);
        this.deleteBooks(Arrays.asList(bookId));
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals("Test title", bookResponseId.getBody().getTitle());
        assertTrue(bookResponseId.getBody().getId() >= 1L);
        assertEquals(1L, bookResponseId.getBody().getComments().size());
        assertEquals(bookId, bookResponseId.getBody().getId());
        assertEquals("test_nick", bookResponseId.getBody().getComments().get(0).getNick());
    }

    @Test
    void given_1_book_with_comments_created_when_call_get_books_should_not_return_json_recursion(){
        ResponseEntity<Void> userCreated = this.userController.createUSer(createTestUser());
        ResponseEntity bookResponse = this.bookController.createBook(createTestBook(new Book("Test title")));
        Long bookId = getIdFromResponse(bookResponse);
        ResponseEntity commentResponse = this.commentController.createComment(bookId, new CommentRequestDto(TEST_NICK
                , "test comment", 3F), this.principal);

        ResponseEntity<Collection<BookResponsePublicDTO>> booksResponse = this.bookController.getBooks(this.principal);
        this.deleteBooks(Arrays.asList(bookId));
        this.userController.deleteUserById(getIdFromResponse(userCreated));

        assertEquals(TEST_GMAIL_COM,
                ((BookResponseDTO)((List<BookResponsePublicDTO>)booksResponse.getBody()).get(0)).getComments().get(0).getEmail());
    }

    @Test
    void given_0_books_created_when_call_book_by_id_should_return_not_found(){
        ResponseEntity<BookResponseDTO> bookResponseId = this.bookController.getBookById(-1L);

        assertEquals(HttpStatus.NOT_FOUND, bookResponseId.getStatusCode());
    }
}
