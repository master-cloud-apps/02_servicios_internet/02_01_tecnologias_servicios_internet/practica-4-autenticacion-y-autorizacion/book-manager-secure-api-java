package org.eyo.bookmanager.services;

import org.eyo.bookmanager.BookBuilder;
import org.eyo.bookmanager.CommentBuilder;
import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.BookRepository;
import org.eyo.bookmanager.repositories.CommentRepository;
import org.eyo.bookmanager.repositories.InMemoryBookRepository;
import org.eyo.bookmanager.repositories.InMemoryCommentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@DataJpaTest
@ActiveProfiles(value = {"h2", "db"})
class CommentDBServiceRepositoryTest {

    @Autowired
    private BookService bookService;
    @Autowired
    private CommentService commentService;

    @Test
    void should_get_empty_comments_list(){
        assertEquals(0, this.commentService.getAll(-1L).size());
    }

    @Test
    void given_new_comment_with_id_zero_when_save_then_new_comment_change_id(){
        Book newBook = new BookBuilder().setTitle("Test Book").buildBook();
        newBook = this.bookService.save(newBook);
        Comment newComment = new CommentBuilder().setContent("Test content").buildComment();
        this.commentService.save(newBook.getId(), newComment);

        assertNotEquals(0L, newComment.getId());
    }

    @Test
    void when_book_with_comments_should_return_all_the_comments_from_book(){
        Book newBook = new BookBuilder().setTitle("Test Book").buildBook();
        this.bookService.save(newBook);
        Comment newComment = new CommentBuilder().setContent("Test content").buildComment();
        this.commentService.save(newBook.getId(), newComment);

        Collection<Comment> comments = this.commentService.getAll(newBook.getId());

        assertEquals(1, comments.size());
    }
}
