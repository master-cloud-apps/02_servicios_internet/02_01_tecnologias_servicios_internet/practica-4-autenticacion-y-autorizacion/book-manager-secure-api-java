package org.eyo.bookmanager.web;

import org.eyo.bookmanager.UserBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.eyo.bookmanager.security.Constants.TOKEN_BEARER_PREFIX;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest
public abstract class WebTest {

    protected Long userId;
    protected String token;
    protected String nick = "nick1";
    protected String email = "test_email@gmail.com";
    protected String password = "test_pass";

    @Autowired
    protected MockMvc mockMvc;

    @BeforeEach
    void setUpUSer() throws Exception {
        this.userId =
                createUser(new UserBuilder()
                        .setNick(nick)
                        .setEmail(email)
                        .setPassword(password).build());
        String login = "{" +
                "\"email\": \"" + email + "\"," +
                "\"password\": \"" + password + "\"" +
                "}";

        MvcResult result = this.mockMvc.perform(
                post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(login))
                .andExpect(status().isOk()).andReturn();

        this.token = result.getResponse().getHeader("Authorization");
        System.out.println(this.token);
    }

    @AfterEach
    void deleteUser() throws Exception {
        this.mockMvc.perform(delete("/users/" + userId)
                .header("Authorization", "Bearer " + this.token));
    }

    protected Long getIdFromLocation(String locationHeader) {
        return Long.parseLong(locationHeader.substring(locationHeader.lastIndexOf('/') + 1));
    }

    protected Long createBook(String bookJson) throws Exception {
        return createEntityAndReturnId(bookJson, "/books");
    }

    protected Long createUser(String userJson) throws Exception {
        return createEntityAndReturnId(userJson, "/users");
    }

    protected Long createComment(String commentJson, Long bookId) throws Exception {
        return createEntityAndReturnId(commentJson, "/books/" + bookId + "/comments");
    }

    protected Long createEntityAndReturnId(String elementJson, String uri) throws Exception {
        if (token == null){
            MvcResult result = this.mockMvc.perform(
                    post(uri)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(elementJson))
                    .andExpect(status().isCreated())
                    .andReturn();
            return getIdFromLocation(result.getResponse().getHeader("location"));
        }
        MvcResult result = this.mockMvc.perform(
                post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(elementJson)
                        .header("Authorization", "Bearer " + token))
                .andExpect(status().isCreated())
                .andReturn();
        return getIdFromLocation(result.getResponse().getHeader("location"));
    }
}
