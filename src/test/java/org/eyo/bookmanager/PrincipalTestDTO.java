package org.eyo.bookmanager;

import java.security.Principal;

public class PrincipalTestDTO implements Principal {
    private String name;

    public PrincipalTestDTO(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object another) {
        return false;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
