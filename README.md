# Book Manager

Proyecto con el manejador de libros online de la primera práctica
de la asignatura de Tecnologías de Servicios de Internet
del Máster de Cloud Apps de la URJC.

## Configuración

Lanzar mysql en el puerto 3406 con la siguiente configuración:

````shell script
docker run --rm -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=posts -p 3406:3306 -d mysql:8.0.22
````

Also you will have to create the `library` schema in the database.

````sql
CREATE SCHEMA `library`;
````

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#using-boot-devtools)
* [Mustache](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-spring-mvc-template-engines)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

